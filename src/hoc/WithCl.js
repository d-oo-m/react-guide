import React, { Component } from 'react';

// const withCl = (WrappedComponent, className) => {
//   return (props) => (
//     <div className={className}>
//       <WrappedComponent {...props} />
//     </div>
//   );
// }

const withCl = (WrappedComponent, className) => {
  const WithCl = class extends Component {
    render () {
      return (
        <div className={className}>
          <WrappedComponent ref={this.props.forwardedRef} {...this.props} />
        </div>
      )
    }
  }

  return React.forwardRef((props, ref) => {
    return <WithCl {...props} forwardedRef={ref} />
  })
}

export default withCl;